//Graphics

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.sound.sampled.*;
import java.util.*;
import java.io.*;
import javax.imageio.ImageIO;

public class SetPassword extends JFrame implements ComponentListener {

   private JPasswordField pass1, pass2;
   private JButton newPass;
   private JPanel mainMenu;
   public final static Color brenan = new Color( 57,255,20 );
   private JTextArea textField5, textField6, textField7, textField8, textField9;
   public static String setPass;
   public Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
   private double height = (int) screenSize.getHeight(), width = (int) screenSize.getWidth();

   public SetPassword() {
      try {
         this.setIconImage(ImageIO.read(new File("res/ic_calendar.png")));
      } 
      catch (Exception e) {
      
      }
      setSize((int) width, (int) height);
      setLayout(null);
      setPassword();
      addComponentListener(this);
   }
   
   public void setPassword() 
   {
      mainMenu = new JPanel();
      mainMenu.setLocation((int)(width/2.205), (int)(height/2.4));
      mainMenu.setSize((int)(width/12), (int)(height/18));
      add(mainMenu);
      pass1 = new JPasswordField(10);
      pass1.setToolTipText("Set Your Password");
      pass2 = new JPasswordField(10);
      pass2.setToolTipText("Verify Your Password");
      mainMenu.add(pass1);
      mainMenu.add(pass2);
      textField5 = new JTextArea();
      textField5.setText("Make Sure Both Passwords Are The Same!");
      textField5.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));
      textField5.setForeground(Color.RED);
      textField5.setLocation((int)(width/2.53), (int)(height/2.7));
      textField5.setSize((int)(width/5), (int)(height/49));
      textField5.setOpaque(false);
      textField5.setEditable(false);
      textField5.setHighlighter(null);
      textField5.setVisible(false);
      add(textField5);
      textField7 = new JTextArea();
      textField7.setText("You Must Enter A Password");
      textField7.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));
      textField7.setForeground(Color.RED);
      textField7.setLocation((int)(width/2.33), (int)(height/1.6));
      textField7.setSize((int)(width/4.8), (int)(height/49));
      textField7.setOpaque(false);
      textField7.setEditable(false);
      textField7.setHighlighter(null);
      textField7.setVisible(false);
      add(textField7);
      textField8 = new JTextArea();
      textField8.setText("Set Password:");
      textField8.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));
      textField8.setForeground(Color.BLACK);
      textField8.setLocation((int)(width/2.6), (int)(height/2.41));
      textField8.setSize((int)(width/5), (int)(height/49));
      textField8.setOpaque(false);
      textField8.setEditable(false);
      textField8.setHighlighter(null);
      add(textField8);
      textField9 = new JTextArea();
      textField9.setText("Verify Password:");
      textField9.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));
      textField9.setForeground(Color.BLACK);
      textField9.setLocation((int)(width/2.67), (int)(height/2.27));
      textField9.setSize((int)(width/4), (int)(height/45));
      textField9.setOpaque(false);
      textField9.setEditable(false);
      textField9.setHighlighter(null);
      add(textField9);
      textField6 = new JTextArea();
      textField6.setText("Create A Password");
      textField6.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/30)));
      textField6.setForeground(Color.BLACK);
      textField6.setLocation((int)(width/2.42), (int)(height/3.1));
      textField6.setSize((int)(width/5), (int)(height/29));
      textField6.setOpaque(false);
      textField6.setEditable(false);
      textField6.setHighlighter(null);
      add(textField6);
      newPass = new JButton("Set Password");
      newPass.setToolTipText("Click This After You Enter Your Password!");
      newPass.setForeground(brenan);
      newPass.setBackground(Color.BLUE);
      newPass.setBorder(BorderFactory.createLineBorder(brenan,2));
      newPass.setFont(new Font("Rockwell", Font.BOLD, (int)(height/50)));
      newPass.setLocation((int)(width/2.477),(int)(height/1.9));
      newPass.setSize((int)(width/5.486),(int)(height/13.5));
      add(newPass);
      getRootPane().setDefaultButton(newPass);
     
      newPass.addActionListener(
            new ActionListener()
            {
               public void actionPerformed(ActionEvent e)
               {
                  String setPass = String.valueOf(pass1.getPassword());
                  String verifyPass = String.valueOf(pass2.getPassword());
                  
                  if(setPass.equals(verifyPass) && !setPass.equals(""))
                  {
                     try{
                        PrintWriter writer = new PrintWriter("TempSave.txt", "UTF-8");
                        writer.println(setPass);
                        writer.close();
                     } 
                     catch (IOException ke) {
                     }         
                     launchIntro();
                  }
                  else
                  {
                     if(setPass.equals("") || verifyPass.equals(""))
                     {
                        textField7.setVisible(true);
                        textField5.setVisible(false);
                     }
                     else
                     {
                        textField5.setVisible(true);
                        textField7.setVisible(false);
                     }
                  } 
               }
            });
      this.setVisible(true);
   }
   private void changeSize() {
      
      textField5.setSize((int)(width/5), (int)(height/49));
      mainMenu.setLocation((int)(width/2.205), (int)(height/2.4));
      mainMenu.setSize((int)(width/12), (int)(height/18));
      textField5.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));
      textField5.setLocation((int)(width/2.53), (int)(height/2.7));
      textField5.setSize((int)(width/5), (int)(height/49));
      textField7.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));
      textField7.setLocation((int)(width/2.33), (int)(height/1.6));
      textField7.setSize((int)(width/4.8), (int)(height/49));
      textField8.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));
      textField8.setLocation((int)(width/2.6), (int)(height/2.41));
      textField8.setSize((int)(width/5), (int)(height/49));
      textField9.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/50)));
      textField9.setLocation((int)(width/2.67), (int)(height/2.27));
      textField9.setSize((int)(width/4), (int)(height/45));
      textField6.setFont(new Font("TimesRoman", Font.BOLD, (int)(height/30)));
      textField6.setLocation((int)(width/2.42), (int)(height/3.1));
      textField6.setSize((int)(width/5), (int)(height/29));
      newPass.setFont(new Font("Rockwell", Font.BOLD, (int)(height/50)));
      newPass.setLocation((int)(width/2.477),(int)(height/1.9));
      newPass.setSize((int)(width/5.486),(int)(height/13.5));
   }
   
   public void componentResized(ComponentEvent e) {
      height = e.getComponent().getHeight();
      width = e.getComponent().getWidth();
      changeSize();
   }
   
   public void componentMoved(ComponentEvent e) {
      
   }
   
   public void componentShown(ComponentEvent e) {
    
   }
   
   public void componentHidden(ComponentEvent e) {
   
   }
   
   private void launchIntro() {
      if (new File("db/calendar.db").exists())
      {
         new Login();
         dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
      }
      else{
         new Intro();
         dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
      }
   }
}